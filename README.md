# Learn Typescript

Following the chapters of the book Essential Typescript 4 from Apress by Adam Freeman

## Chapter 1 notes - Typscript Setup

### 1. Automatically compile and run on change of .ts files

Track file change with nodemon and launch incremental typescript complilation then run of the compliled scripted.

Add the 'dev' script to package.json :

```json
    "scripts": {
        "dev": "nodemon -e ts --watch src --exec 'tsc --incremental && node dist/index.js'",
    },
```

Source: https://stackoverflow.com/questions/38276862/is-there-a-way-to-use-npm-scripts-to-run-tsc-watch-nodemon-watch

### 2. Inquirer - Add prompt functions to the terminal

When working with nodemon as described above, we need to use the nodemon "-I" option to send keystrokes to the inquirer
library functions

```json
    "scripts": {
        "dev": "nodemon -I -e ts --watch src --exec 'tsc --incremental && node dist/index.js'",
    },
```

Source: https://github.com/SBoudrias/Inquirer.js/issues/586

### 3. Add Types to npm packages - The Definitely Typed Project

To provide type information about npm packages, search and install as devDependency, a type package named :

@types/[npm_package_name]

### 4. Enable VScode builtin Typescript and Javascript Languages features

Open the Extensions panel
Type in the search field `@builtin typescript`
Enable the `Typescript and Javascript Language features` which is disabled by default.

VSCode should now take into account @types/[package] npm modules provides type information for third party npm modules and provide syntax highlight when writing code.

The extension can pick up errors that is not raised by the `tsc` compiler. For example, use of additional and not expected function parameters.

**For information**: the global $PATH variable used to tell where to look for globally installed programs is in the `/etc/paths` file. Also check other .profile or .\*\*rc files to check for additions to this list of installed package folders.

Source: https://www.skymac.org/Univers-Apple/article-4eb0d478-Modifier-le-path-de-votre-terminal-sur-macOS.htm

**For information**: Globally installed packages with npm can be found at this path `/usr/local/lib/node_modules`

**For information**: if a package is in a node_modules folder, you can remove it manually with `rm -rf [folder_name]`.

### 5. tsc-watch - nodemon for typescript

tsc-watch needs typescript as a peer dependency to work. A globally installed typescript cannot work with it. Install typescript as a devdependency in the project to use tsc-watch

the script to add to package.json could be :

```json
    "scripts": {
        "start": "tsc-watch --onSuccess 'node dist/index.js'",
    }
```

By this way, if there is any compilation error detected by tsc, the application will not be launched.

The error detected by the tsc compiler should be the same as the one detected by the Typescript/Javascript Language features from VSCode.

## Chapter 3 notes - Javascript Primer - Part 1

### 1. Setup Typescript Project References to use smaller units

At the root of the project, create a tsconfig.json that "references" sub folders in the project

```json
{
  "compilerOptions": {
    "target": "ES2018",
    "module": "commonJS"
  },
  "references": [
    {
      "path": "./chapters/chap1"
    },
    {
      "path": "./chapters/chap3"
    }
  ]
}
```

In each sub folder, a separate tsconfig.json needs to be created :

In the chap1 sub folder :

```json
{
  "extends": "../../tsconfig.json",
  "compilerOptions": {
    "outDir": "../../dist/chap1",
    "composite": true,
    "declaration": true,
    "declarationMap": true,
    "noEmitOnError": true
    // "sourceMap": true
  },
  "include": ["src/**/*.ts"]
}
```

In the chap3 sub folder :

```json
{
  "extends": "../../tsconfig.json",
  "compilerOptions": {
    "outDir": "../../dist/chap3",
    "composite": true,
    "declaration": true,
    "declarationMap": true,
    "noEmitOnError": true
    // "sourceMap": true
  },
  "include": ["src/**/*.ts"]
}
```

In the root folder, we can add a script to build the project whenever any file changes with :

```
tsc --build --watch
```

or

```
tsc -b -w
```

**Beware** : current usage of Typescript Project References functionality will also create an compiled index.js in the 'ts' source folder. This issue is still being investigated for future correction.

### 2. Type coersion in javascript

When comparing with the "==" equality operator a number variable and a string variable :

- string will be converted in a number to make the comparison.

When the addition "+" operator is used:

- the number will be converted in a string and concatened to the other variable.
- `undefined` values will be converted in `NaN` special number value. An addition operation where of the terms is `NaN` will produce a `NaN` result.

### 3. Nullish Coalescing operator

Sometimes, checking the truthy/falsy nature of a variable to return a value may not be enough.

```js
let firstValue;
let secondValue = firstValue || "initial value";
```

Here firstValue is `undefined` as it has not be initialized after its creation.
The logical `||` 'OR' operator checks the truthiness of firstValue. As the value is not initialized, the default value "initial value" is used.

```js
let rate = 0;
let rateMessage = `the rate to apply is ${rate || 10}%`;
```

In the above example, the value of `rate` is intended to be `0` but as it will be considered falsy :

- 0 is converted to "", an empty string
- the special number `NaN` (Not a Number) is converted to `false`.
  then, the value `10` will be used instead.

To really check for the presence of an undefined or null value, we use the null coalescing operator `??`:

```js
let rate = 0;
let rateMessage = `the rate to apply is ${rate ?? 10}%`;
```

Only if the variable `rate` is not initialized or has `null` as value will the value `10` be used.
In our case, the value `0` will be used as intended.

### 4. Working with objects

Create an object literal

```js
let myObject = {
  prop1: "hello",
  prop2: "world",
  times: 3,
};
```

Add a new property to an object : `myObject.newProperty = propertyValue`.
Remove a property from an object : `delete myObject.prop1`.

Accessing to an object property that is nonexistent returns `undefined`
Setting or changing a property that is nonexistent will add a new property to the object.

Use the spread operator to copy an object `let newObject = { ...initialObject }`.

### 5. Object getters and setters

Getters and setters are considered properties of an object so they are separated from one another with commas.
Since they are property, even if thery are defined by functions, they cannot have the same name as another object
property.

Javascript does not provide a private property functionality, i.e. property that is not accessible from the outside.
Property names prefixed with the 'underscore' character is a naming convention to declare that a variable should not be
directly called from the outside of the object.

The properties will be modiified by getters and setters

```js
let myObject {
  name: "myObject",
  _times: 0,
  message: "You have called 0 times",

  get times() {
    return _times;
  },

  set times(newValue) {
    this._times = newValue;
    this.message = `You have called ${this._times} times`;
  },

  get numberTimes() {
    return this.message;
  },
}
```

### 6. Object methods

Object methods are object properties which value is a function.
Here is a method definition using the `function` keyword:

```js
let myObject2 = {
  name: "myObject",
  _times: 0,
  message: "You have called 0 times",

  get numberTimes() {
    return `myObject has been called ${this._times} times`;
  },

  writeDetails: function () {
    return `myObject name:${this.name} message:${this.numberTimes}`;
  },
};
```

Other possible definition for an object method without the function keyword:

```js
let myObject2 = {
  name: "myObject",
  _times: 0,
  message: "You have called 0 times",

  get numberTimes() {
    return `myObject has been called ${this._times} times`;
  },

  writeDetails() {
    return `myObject name:${this.name} message:${this.numberTimes}`;
  },
};
```

### 7. the This keyword

The `this` keyword can be used in any function, even if the function is not a method of an object.

In a standalone function, the `this` keyword references/points to the :

- **global** object in Node runtime environment
- the **document or DOM** in browser runtime environment

For a method, the `this` keyword when used inside the function will refer to :

- the object where the method is defined when the method is called from the object with the 'dot' notation
  example: `myObject.methodfunction()`
- the **global** object or the **document or DOM** when the method is extracted from the object and assigned to a
  variable `const newFunction = myObject.methodFunction`

Note: when in javascript script `use strict mode` is used, the `this` keyword of standalone function takes the value of `undefined`.

As functions are also objects in javascript, they have a `call` method that is used to invoke a function and set the value of `this` inside a function, whether it is a standalone function or an extracted object method.

```js
myFunction.call(sourceObject);
```

`this` inside the function will reference `sourceObject`.

As the `call` method of a function must be called each time the function is invoke, it would be more practical to be able to fix the value of `this` inside a function for any further call.

To that effect, we use the function object `bind` method.

```js
myObject.methodFunction = myObject.methodFunction.bind(myObject);
```

By this way, even if method is extracted from an object and assigned to a variable, when the function is executed, the `this` keyword will always point to same object.

### 8. the `this` keyword in arrow function

The arrow functions can also use the `this` keyword but its value refers to the value of `this` that can be found in the closest scope surrounding the function when it is executed.

So we cannot bind an object to an arrow function, due to their dynamic reference of `this`.
