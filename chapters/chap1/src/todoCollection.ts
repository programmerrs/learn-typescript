import { TodoItem } from "./todoItem";

// Type defining the "shape" of object literals in javascript
type ItemCounts = {
  total: number;
  incomplete: number;
};

export class TodoCollection {
  private nextId: number = 1;
  // access control of itemMap is set to "protected" to allow this class or subclasses
  // to have access to its value
  protected itemMap = new Map<number, TodoItem>();

  // Removing the access control keyword 'public' from todoItems will remove
  // todoItems from the class instance properties
  // todoItems is read to initialize the private property itemMap
  // Map is more efficient that Javascript objects when there is a lot
  // of addition/deletion operation. Js objects is faster for reads.
  // Map is not limited as Js Object to use string or number only as keys for
  // its properties.
  constructor(public userName: string, todoItems: TodoItem[] = []) {
    todoItems.forEach((item) => this.itemMap.set(item.id, item));
  }

  addTodo(task: string): number {
    while (this.getTodoById(this.nextId)) {
      this.nextId++;
    }
    this.itemMap.set(this.nextId, new TodoItem(this.nextId, task));
    return this.nextId;
  }

  getTodoById(id: number): TodoItem {
    return this.itemMap.get(id);
  }

  getTodoItems(includeComplete: boolean): TodoItem[] {
    return [...this.itemMap.values()].filter(
      (item) => includeComplete || !item.complete
    );
  }

  removeComplete() {
    this.itemMap.forEach((item) => {
      if (item.complete) {
        this.itemMap.delete(item.id);
      }
    });
  }

  markComplete(id: number, complete: boolean) {
    const todoItem = this.getTodoById(id);
    if (todoItem) {
      todoItem.complete = complete;
    }
  }

  getItemCounts(): ItemCounts {
    return {
      total: this.itemMap.size,
      incomplete: this.getTodoItems(false).length,
    };
  }
}
