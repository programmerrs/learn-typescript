// Using object getters and setters
console.log("myObject");
let myObject = {
  name: "myObject",
  _times: 0,
  message: "You have called 0 times",

  get times() {
    return this._times;
  },

  set times(newValue) {
    this._times = newValue;
    this.message = `You have called ${this._times} times`;
  },

  get numberTimes() {
    return `Number of times ${this._times}`;
  },
};

// The "privatized" _times variable can still be accessed and printed
console.log(myObject._times);
console.log(myObject.numberTimes);

// The setter and getter "times" are used to change the internal _times variable
myObject.times = 5;
console.log(myObject.times);
console.log(myObject.numberTimes);
console.log("\n");

console.log("myObject2");
let myObject2 = {
  name: "myObject2",
  _times: 0,
  message: "You have called 0 times",

  get numberTimes() {
    return `myObject2 has been called ${this._times} times`;
  },

  set times(newValue) {
    this._times = newValue;
  },

  writeDetails: function () {
    return `myObject2 name: ${this.name} message: ${this.numberTimes}`;
  },

  says() {
    return this.message;
  },
};

myObject2.times = 3;
console.log(myObject2._times);
console.log(myObject2.writeDetails());
console.log("This is what myObject2 has to say ", myObject2.says());
